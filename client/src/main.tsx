import React from 'react'
import ReactDOM from 'react-dom/client'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import './index.css'
import SecretView from './routes/secret';
import { Login_recruter, Login_company } from './routes/login';
import { GoogleOAuthProvider } from '@react-oauth/google';
import { AuthService } from '@genezio/auth';

// Change <token> and <region> with your own values!
AuthService.getInstance().setTokenAndRegion("0-vwjezyek6mrjqvl73eztski5xm0rkwzz", "us-east-1");

const router = createBrowserRouter([
  {
    path: "/",
    element: <SecretView />,
  },
  {
    path: "/login_recruter",
    element: <Login_recruter />,
  },
  {
    path: "/login_company",
    element: <Login_company />,
  },
]);

// Change <google_id> with your own value!
ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
  <GoogleOAuthProvider clientId="174187065934-tkaluqcgv1m2bsq7joce6rlcd6t9hncc.apps.googleusercontent.com">
      <RouterProvider router={router} />
  </GoogleOAuthProvider>
  </React.StrictMode>
)
