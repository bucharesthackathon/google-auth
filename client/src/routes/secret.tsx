import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';


const SecretView: React.FC = () => {
  // @ts-ignore
  const navigate = useNavigate();
  // @ts-ignore
  const [secret, setSecret] = useState('');

  const login_recruter = () => {
    navigate('/login_recruter');
  }

  const login_company = async () => {
    navigate('/login_company');
  }

  return (
    <div className="form-container">
      {secret ? <p>{secret}</p> : <p></p>}
      <button onClick={login_recruter}>Login Recruiter</button>
      <button onClick={login_company}>Login Company</button>
    </div>
  );
};

export default SecretView;
