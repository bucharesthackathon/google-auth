import React, { useState } from 'react';
import { CredentialResponse, GoogleLogin } from '@react-oauth/google';
import { AuthService } from '@genezio/auth';
import "./styles.css";


const Login_recruter: React.FC = () => {
  
  const [googleLoginLoading, setGoogleLoginLoading] = useState(false);

  const handleGoogleLogin = async (credentialResponse: CredentialResponse) => {
      setGoogleLoginLoading(true);
      try {
        await AuthService.getInstance().googleRegistration(credentialResponse.credential!)

        console.log('Login Success');
        window.location.href = "http://130.61.191.54:80";
      } catch(error: any) {
        console.log('Login Failed', error);
        alert('Login Failed');
      }

      setGoogleLoginLoading(false);
  };

  return (
    <div className="form-container">
      { googleLoginLoading ?
            <>Loading...</> :
            <GoogleLogin
                onSuccess={credentialResponse => {
                    handleGoogleLogin(credentialResponse);
                    alert('Login Successful')
                }}
                onError={() => {
                    console.log('Login Failed');
                    alert('Login Failed')
                }}
            />
       }
    </div>
  );
};

const Login_company: React.FC = () => {
  
  const [googleLoginLoading, setGoogleLoginLoading] = useState(false);

  const handleGoogleLogin = async (credentialResponse: CredentialResponse) => {
      setGoogleLoginLoading(true);
      try {
        await AuthService.getInstance().googleRegistration(credentialResponse.credential!)

        console.log('Login Success');
        window.location.href = "http://130.61.191.54:80";
      } catch(error: any) {
        console.log('Login Failed', error);
        alert('Login Failed');
      }

      setGoogleLoginLoading(false);
  };

  return (
    <div className="form-container">
      { googleLoginLoading ?
            <>Loading...</> :
            <GoogleLogin
                onSuccess={credentialResponse => {
                    handleGoogleLogin(credentialResponse);
                    alert('Login Successful')
                }}
                onError={() => {
                    console.log('Login Failed');
                    alert('Login Failed')
                }}
            />
       }
    </div>
  );
};

export { Login_recruter, Login_company };